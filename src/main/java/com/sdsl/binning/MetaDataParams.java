/**
 * 
 */
package com.sdsl.binning;

import java.util.Map;

/**
 * @author Sudip Das
 *
 */
public class MetaDataParams {
	private static final String METADATA_KEY_STR = "metaDataParamsKey";

	private Map<String, String> metaDataParams;

	public Map<String, String> getMetaDataParams() {
		return metaDataParams;
	}

	public void setMetaDataParams(Map<String, String> meta_data) {
		this.metaDataParams = meta_data;
	}

	public static String getMetaDataParamsKeyStr() {
		return METADATA_KEY_STR;
	}
}
