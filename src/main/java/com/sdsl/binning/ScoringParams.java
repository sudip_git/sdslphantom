/**
 * 
 */
package com.sdsl.binning;

import java.util.List;
import java.util.Map;

/**
 * @author Sudip Das
 *
 */
public class ScoringParams {
	private static final String SCOREPARAMS_KEY_STR = "scoringParamsKey";

	private List<Map<String, String>> scoringParams;

	public List<Map<String, String>> getScoringparams() {
		return scoringParams;
	}

	public void setScoringparams(List<Map<String, String>> scoreParams) {
		this.scoringParams = scoreParams;
	}

	public static String getScoreParamsKeyStr() {
		return SCOREPARAMS_KEY_STR;
	}

}
