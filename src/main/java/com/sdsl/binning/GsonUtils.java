/**
 * 
 */
package com.sdsl.binning;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.sdsl.utils.BinningUtil;
import com.sdsl.utils.JsonUtil;

/**
 * @author Sudip Das
 *
 */
public class GsonUtils {

	/**
	 * Method to get a MAP with MetaDataParams, ScoringParams and ComparisonParams
	 * 
	 * @param jsonStr
	 * @param binningNode
	 * @return
	 * @throws ParseException
	 */
	public static LinkedHashMap<String, LinkedHashMap<String, Object>> getMapOfAllBinParamMapsFromJSON(String jsonStr,
			String binningNode) throws ParseException {

		LinkedHashMap<String, LinkedHashMap<String, Object>> binningMap = new LinkedHashMap<String, LinkedHashMap<String, Object>>();
		LinkedHashMap<String, Object> binParamsMap = new LinkedHashMap<String, Object>();

		ArrayList<String> binningQsKeys = JsonUtil.get2LevelKeysNestedJson(jsonStr, binningNode);

		for (String eachBinningQs : binningQsKeys) {
			LinkedHashMap<String, String> qsParamNodes = JsonUtil.get3LevelJsonObjAsStr(jsonStr, binningNode,
					eachBinningQs);
			for (String eachParamNode : qsParamNodes.keySet()) {
//				System.err.println(eachParamNode);

				Gson gson = new Gson();
				MetaDataParams metaDataParamsObj = gson.fromJson(qsParamNodes.get(eachParamNode), MetaDataParams.class);
				binParamsMap.put(MetaDataParams.getMetaDataParamsKeyStr(), metaDataParamsObj);

				ScoringParams scoreParamsObj = gson.fromJson(qsParamNodes.get(eachParamNode), ScoringParams.class);
				binParamsMap.put(ScoringParams.getScoreParamsKeyStr(), scoreParamsObj);

				ComparisonParams compParamsObj = gson.fromJson(qsParamNodes.get(eachParamNode), ComparisonParams.class);
				binParamsMap.put(ComparisonParams.getCompareParamsKeyStr(), compParamsObj);

				binningMap.put(eachParamNode, binParamsMap);
			}
		}
//		System.out.println("binningMap:" + binningMap);
		return binningMap;
	}

	/**
	 * Method to get a LIST of MetaDataParams, ScoringParams and ComparisonParams in
	 * that order Simplified 6.x change
	 * 
	 * @param jsonStr
	 * @param binningNodeStr
	 * @param qsNodeStr
	 * @return
	 * @throws ParseException
	 */
	public static List<Object> getListOfAllBinParamMaps(String jsonStr, String binningNodeStr, String qsNodeStr)
			throws ParseException {
		Gson gson = new Gson();
		
		JSONObject chkLstJsonObj = (JSONObject) new JSONParser().parse(jsonStr);
		JSONObject binningJsonObj = (JSONObject) chkLstJsonObj.get(binningNodeStr);
		JSONObject qsJsonObj=(JSONObject) binningJsonObj.get(qsNodeStr);
		//Need to convert to String to convert JSON to Gson POJO or JsonElement
		String qsJsonStr=gson.toJson(qsJsonObj);

		List<Object> binObj = new ArrayList<Object>();
		LinkedHashMap<String, LinkedHashMap<String, Object>> binningMap = new LinkedHashMap<String, LinkedHashMap<String, Object>>();
		LinkedHashMap<String, Object> binParamsMap = new LinkedHashMap<String, Object>();

		
		MetaDataParams metaDataParamsObj = gson.fromJson(qsJsonStr, MetaDataParams.class);
		Map<String, String> metaDataParams = metaDataParamsObj.getMetaDataParams();
		binParamsMap.put(MetaDataParams.getMetaDataParamsKeyStr(), metaDataParams);
//		System.out.println("GsonUtils metaDataParams:" + metaDataParams);

		ScoringParams scoreParamsObj = gson.fromJson(qsJsonStr, ScoringParams.class);
		List<Map<String, String>> scoreParams = scoreParamsObj.getScoringparams();
		binParamsMap.put(ScoringParams.getScoreParamsKeyStr(), scoreParams);
//		System.err.println("scoreParams:" + scoreParams);

		ComparisonParams compParamsObj = gson.fromJson(qsJsonStr, ComparisonParams.class);
		List<Map<String, String>> compParams = compParamsObj.getComparisonParams();
		binParamsMap.put(ComparisonParams.getCompareParamsKeyStr(), compParams);
//		System.out.println("compParams:" + compParams);
		binningMap.put(qsNodeStr, binParamsMap);

		binObj.add(metaDataParamsObj);
		binObj.add(scoreParamsObj);
		binObj.add(compParamsObj);
//		System.err.println("binningMap:" + binningMap);
		return binObj;
	}

	/**
	 * Method to get a LIST of MetaDataParams, ScoringParams and ComparisonParams in
	 * that order
	 * 
	 * @param jsonStr
	 * @param binningNode
	 * @param refSubNode
	 * @return
	 * @throws ParseException
	 */
	public static List<Object> getListOfAllBinParamMapsFromJSON(String jsonStr, String binningNode, String refSubNode)
			throws ParseException {

		List<Object> binObj = new ArrayList<Object>();
		LinkedHashMap<String, LinkedHashMap<String, Object>> binningMap = new LinkedHashMap<String, LinkedHashMap<String, Object>>();
		LinkedHashMap<String, Object> binParamsMap = new LinkedHashMap<String, Object>();

		LinkedHashMap<String, String> qsParamNodes = JsonUtil.get3LevelJsonObjAsStr(jsonStr, binningNode, refSubNode);
		for (String eachParamNode : qsParamNodes.keySet()) {
//				System.err.println("GsonUtils eachParamNode:"+eachParamNode);

			Gson gson = new Gson();
			MetaDataParams metaDataParamsObj = gson.fromJson(qsParamNodes.get(eachParamNode), MetaDataParams.class);
			Map<String, String> metaDataParams = metaDataParamsObj.getMetaDataParams();
			binParamsMap.put(MetaDataParams.getMetaDataParamsKeyStr(), metaDataParams);
//				System.out.println("GsonUtils metaDataParams:" + metaDataParams);

			ScoringParams scoreParamsObj = gson.fromJson(qsParamNodes.get(eachParamNode), ScoringParams.class);
			List<Map<String, String>> scoreParams = scoreParamsObj.getScoringparams();
			binParamsMap.put(ScoringParams.getScoreParamsKeyStr(), scoreParams);
//				System.err.println("scoreParams:" + scoreParams);

			ComparisonParams compParamsObj = gson.fromJson(qsParamNodes.get(eachParamNode), ComparisonParams.class);
			List<Map<String, String>> compParams = compParamsObj.getComparisonParams();
			binParamsMap.put(ComparisonParams.getCompareParamsKeyStr(), compParams);
//				System.out.println("compParams:" + compParams);
			binningMap.put(eachParamNode, binParamsMap);

			binObj.add(metaDataParamsObj);
			binObj.add(scoreParamsObj);
			binObj.add(compParamsObj);
		}
//		}
//		System.out.println("binningMap:" + binningMap);
		return binObj;
	}

	/**
	 * Get a list of scoring parameters or comparison parameters. Ignores meta data
	 * 
	 * @param gsonObj
	 * @return
	 * @throws ParseException
	 */

	public static List<String> getBinJsonTemplateKeys(Object gsonObj) throws ParseException {

		List<String> tmplParamKeys = new ArrayList<String>();

		if (gsonObj instanceof ScoringParams) {
			ScoringParams scoreParamsObj = (ScoringParams) gsonObj;
			List<Map<String, String>> scoreParamMapsList = scoreParamsObj.getScoringparams();
			for (Map<String, String> eachScoreParamMap : scoreParamMapsList) {
				tmplParamKeys.add(eachScoreParamMap.get("id"));
			}
		} else if (gsonObj instanceof ComparisonParams) {
			ComparisonParams compParamsObj = (ComparisonParams) gsonObj;
			List<Map<String, String>> compareParamMapsList = compParamsObj.getComparisonParams();
			for (Map<String, String> eachCompareParamMap : compareParamMapsList) {
				tmplParamKeys.add(eachCompareParamMap.get("id"));
			}
		}

		return tmplParamKeys;
	}

	/**
	 * Method to get a portion of a JSON String using an element name.
	 * 
	 * @param jsonStr     The original JSON String
	 * @param elementName The JSON Element of interest
	 * @return String associated to the Element of interest
	 */
	public static String getJsonPrimitiveFromJson(String jsonStr, String elementName) {
		String returnStr = "";
		JsonParser parser = new JsonParser();
		JsonElement jsonTree = parser.parse(jsonStr);
		if (jsonTree.isJsonObject()) {
			JsonObject jsonObject = jsonTree.getAsJsonObject();

			JsonElement jsonElement = jsonObject.get(elementName);

			if (jsonElement.isJsonPrimitive()) {
				JsonPrimitive jsonPrimitive = jsonElement.getAsJsonPrimitive();
				returnStr = jsonPrimitive.getAsString();
			}

		}
		return returnStr;
	}
//
//	@SuppressWarnings("unchecked")
//	public static HashMap<String, Object> getValueOfMatchedTemplateKeys(HashMap<String, Object> dbDataMap,
//			List<String> templateParamKeys) {
//		// For scoring
//		HashMap<String, Object> finalValues = new HashMap<String, Object>();
//		for (String eachTemplateParamKey : templateParamKeys) {
//			Object dbObjValue = dbDataMap.get(eachTemplateParamKey);
//			if (dbObjValue == null) {
////				System.out.println(eachTemplateParamKey + " is null");
//				finalValues.put(eachTemplateParamKey, "");
//			} else if (dbObjValue instanceof HashMap) {
////				System.out.println(eachTemplateParamKey + " is map");
//				HashMap<String, String> currentScoreParamMap = (HashMap<String, String>) dbDataMap
//						.get(eachTemplateParamKey);
//				finalValues.put(eachTemplateParamKey, currentScoreParamMap.get("value_std"));
//			} else {
////				System.out.println(eachTemplateParamKey + " is not map");
//				finalValues.put(eachTemplateParamKey, dbObjValue.toString());
//			}
//		}
//		return finalValues;
//	}

	/**
	 * @param args
	 * @throws IOException
	 * @throws ParseException
	 */
	public static void main(String[] args) throws IOException, ParseException {
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		String jsonStr = JsonUtil.getJSonToStr(classloader, "exdionChecklist.json");
		String qs_key = "IE1";

		List<Object> qsObjectsFrmJSON = getListOfAllBinParamMapsFromJSON(jsonStr, "binningrecord", qs_key);

		MetaDataParams metaDataParamsObj = (MetaDataParams) qsObjectsFrmJSON.get(0);
		ScoringParams scoreParamsObj = (ScoringParams) qsObjectsFrmJSON.get(1);
		ComparisonParams compParamsObj = (ComparisonParams) qsObjectsFrmJSON.get(2);

		List<String> scoreTemplateParamKeys = getBinJsonTemplateKeys(scoreParamsObj);
		List<String> compareTemplateParamKeys = getBinJsonTemplateKeys(compParamsObj);

		// Testing using dummy JSON
		String dbJSONStr = JsonUtil.getJSonToStr(classloader, "binning.json");

		HashMap<String, Object> dbDataMap = JsonUtil.getMixedDBJsonMap(dbJSONStr);
		// Note lob value is not available in DBJSON so it is packed with empty string
		System.out.println("Scoring Values from DB JSON:"
				+ BinningUtil.getValueOfMatchedTemplateKeys(dbDataMap, scoreTemplateParamKeys));
		System.out.println("Compare Values from DB JSON:"
				+ BinningUtil.getValueOfMatchedTemplateKeys(dbDataMap, compareTemplateParamKeys));

		System.out.println("Metadata Values from DB JSON:" + metaDataParamsObj.getMetaDataParams());

//		ArrayList<String> temp=new ArrayList<String>();
//		temp.add("a");
//		temp.add("b");
//		temp.add("c");
////		temp.add("d");
//		
//		HashMap <String, Double> weights=new HashMap<String, Double>();
//		
//		int number=temp.size();
//		double distribute=1.0/number;
//		distribute=Math.round(distribute * 100.0) / 100.0;
//		
//		if((number & 1) == 0) {
//			for(String eachTemp: temp) {
//				weights.put(eachTemp, distribute);
//			}
//			System.out.println("Even");
//			System.out.println(weights);
//		}else {
//			double oddTotal=0.0;
//			for(String eachTemp: temp) {
//				weights.put(eachTemp, distribute);
//				oddTotal=oddTotal+distribute;
//			}
//
//			double residual=distribute+1.0-oddTotal;
//			residual=Math.round(residual * 100.0) / 100.0;
//			if(StringUtil.containsStringIgnoreCase("A",temp)) {
//				weights.put("a", residual);				
//			}
//			System.out.println("Odd");
//			System.out.println(weights);
//		}

	}

}
