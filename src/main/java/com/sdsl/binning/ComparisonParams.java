/**
 * 
 */
package com.sdsl.binning;

import java.util.List;
import java.util.Map;

/**
 * @author Sudip Das
 *
 */
public class ComparisonParams {
	private static final String COMPAREPARAMS_KEY_STR = "comparisonParamsKey";

	private List<Map<String, String>> comparisonParams;

	public List<Map<String, String>> getComparisonParams() {
		return comparisonParams;
	}

	public void setComparisonparams(List<Map<String, String>> compParams) {
		this.comparisonParams = compParams;
	}

	public static String getCompareParamsKeyStr() {
		return COMPAREPARAMS_KEY_STR;
	}

}
