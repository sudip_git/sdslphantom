/**
 * 
 */
package com.sdsl.utils;

import java.util.HashMap;
import java.util.List;

import org.json.simple.parser.ParseException;

import com.sdsl.binning.ComparisonParams;
import com.sdsl.binning.GsonUtils;
import com.sdsl.binning.MetaDataParams;
import com.sdsl.binning.ScoringParams;

/**
 * @author Sudip Das
 *
 */
public class BinningUtil {
	private MetaDataParams metaDataParamsObj = null;
	private ScoringParams scoreParamsObj = null;
	private ComparisonParams compParamsObj = null;

	private List<String> scoreTemplateParamKeys = null;
	private List<String> compareTemplateParamKeys = null;

	public BinningUtil(String jsonStr, String recordType, String qsKey) throws ParseException {
		// 6.x change
//		List<Object> qsObjectsFrmJSON = GsonUtils.getListOfAllBinParamMapsFromJSON(jsonStr, recordType, qsKey);
		List<Object> qsObjectsFrmJSON = GsonUtils.getListOfAllBinParamMaps(jsonStr, recordType, qsKey);
		metaDataParamsObj = (MetaDataParams) qsObjectsFrmJSON.get(0);
		scoreParamsObj = (ScoringParams) qsObjectsFrmJSON.get(1);
		compParamsObj = (ComparisonParams) qsObjectsFrmJSON.get(2);
	}

//	public BinningUtil(LinkedHashMap<String, Object> binQsPropsObjs) throws ParseException {
//		this.metaDataParamsObj = (MetaDataParams) binQsPropsObjs.get(MetaDataParams.getMetaParams());
//		this.scoreParamsObj = (ScoringParams) binQsPropsObjs.get(ScoringParams.getScoreParams());
//		this.compParamsObj = (ComparisonParams) binQsPropsObjs.get(ComparisonParams.getCompareParams());
//
//		this.scoreTemplateParamKeys = GsonUtils.getBinningGsonTemplateKeys(scoreParamsObj);
//		this.compareTemplateParamKeys = GsonUtils.getBinningGsonTemplateKeys(compParamsObj);
//
////		System.err.println("metaDataParamsObj:" + metaDataParamsObj);
////		System.err.println("scoreParamsObj:" + scoreParamsObj);
////		System.err.println("compParamsObj:" + compParamsObj);
//	}

	public MetaDataParams getMetaDataParamsObj() {
		return metaDataParamsObj;
	}

	public ScoringParams getScoreParamsObj() {
		return scoreParamsObj;
	}

	public ComparisonParams getCompParamsObj() {
		return compParamsObj;
	}

	public List<String> getScoreTemplateParamKeys() {
		return scoreTemplateParamKeys;
	}

	public List<String> getCompareTemplateParamKeys() {
		return compareTemplateParamKeys;
	}

	public static MetaDataParams getMetaDataParams() {
		return null;
	}

	/**
	 * TO DO: Check if this method is being used.
	 */

	@SuppressWarnings("unchecked")
	public static HashMap<String, Object> getValueOfMatchedTemplateKeys(HashMap<String, Object> dbDataMap,
			List<String> templateParamKeys) {
		// For scoring
		HashMap<String, Object> finalValues = new HashMap<String, Object>();
		for (String eachTemplateParamKey : templateParamKeys) {
			Object dbObjValue = dbDataMap.get(eachTemplateParamKey);
			if (dbObjValue == null) {
//				System.out.println(eachTemplateParamKey + " is null");
				finalValues.put(eachTemplateParamKey, "");
			} else if (dbObjValue instanceof HashMap) {
//				System.out.println(eachTemplateParamKey + " is map");
				HashMap<String, String> currentScoreParamMap = (HashMap<String, String>) dbDataMap
						.get(eachTemplateParamKey);
				finalValues.put(eachTemplateParamKey, currentScoreParamMap.get("value_std"));
			} else {
//				System.out.println(eachTemplateParamKey + " is not map");
				finalValues.put(eachTemplateParamKey, dbObjValue.toString());
			}
		}
		return finalValues;
	}

}
