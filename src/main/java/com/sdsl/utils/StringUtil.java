/**
 * Copyright (C) 2018, Sumyag Data Sciences Pvt. Ltd - www.sumyag.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Sumyag Data Sciences Private Limited, 
   associated partners and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

 */
package com.sdsl.utils;

import java.util.ArrayList;

import com.isotropic.utils.IsotropicStringSimilarity;

/**
 * @author Sudip Das
 *
 */
public class StringUtil {

	/*
	 * Get standard value, trim and convert to upper case. Steps included to remove
	 * leading and trailing spaces and other unnecessary characters within the
	 * strings as match was getting affected by spaces inside strings (question mark
	 * cannot be taken out: regex error)
	 */
	/**
	 * Method to remove special characters from a String and convert to upper case.
	 * This method is called before any string comparison
	 * 
	 * @param rawString String to be converted
	 * @return Normalized upper case String
	 */
	public static String getNormalizedUpCaseString(String rawString) {
		String returnStr = "";
		if (rawString != "" || rawString != null) {
//			returnStr = rawString.trim().replaceAll("[^A-Za-z0-9 ]", "");
			returnStr = rawString.trim();
			returnStr = returnStr.toUpperCase();
		}
		return returnStr;
	}

	/**
	 * Method to return the score of two Strings comparison.
	 * 
	 * @param str1 First String
	 * @param str2 Second String
	 * @return score
	 */
	public static double getStringMatchScore(String str1, String str2, ArrayList<String> ignoreStrList) {
		if (str1.length() == 0 || str2.length() == 0) {
			return 0.0;
		} else if (containsStringIgnoreCase(str1, ignoreStrList) || containsStringIgnoreCase(str2, ignoreStrList)) {
			return 0.0;
		} else {
			return IsotropicStringSimilarity.similarity(str1, str2);
		}
	}

	public static boolean containsStringIgnoreCase(String inputStr, ArrayList<String> ignoreStrList) {
		if (inputStr.trim().length() == 0 || ignoreStrList.stream().anyMatch(inputStr::equalsIgnoreCase)) {
			return true;
		} else
			return false;
	}
	// Place holder for reading Json to be tested
}
