/**
 * Copyright (C) 2018, Sumyag Data Sciences Pvt. Ltd - www.sumyag.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Sumyag Data Sciences Private Limited, 
   associated partners and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

 */
package com.sdsl.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.javatuples.Pair;

import com.isotropic.utils.IsotropicStringSimilarity;

/**
 * @author Sudip Das
 *
 */
public class CollectionsUtil {

	/**
	 * Method to remove items from a list after sorting based on a reference list of
	 * indices.
	 * 
	 * @param recordsList   List of records from which items have to be removed
	 * @param removeIdxList List of indices used for removal of items
	 * @return List with records removed
	 */
	public static ArrayList<LinkedHashMap<String, String>> removeDuplicatesFromList(
			ArrayList<LinkedHashMap<String, String>> recordsList, ArrayList<Integer> removeIdxList) {

		Set<Integer> dedupedList = new LinkedHashSet<Integer>(removeIdxList);
		removeIdxList.clear();
		removeIdxList.addAll(dedupedList);

		Collections.sort(removeIdxList);

		for (int i = removeIdxList.size() - 1; i >= 0; i--) {
			recordsList.remove(removeIdxList.get(i).intValue());
		}
		return recordsList;
	}

	/**
	 * Method that splits a List with maps and returns two Lists with maps as a Pair
	 * tuple. The first List has maps based on a String match, the second list has
	 * map which did not pass the String match
	 * 
	 * @param mapList   Original ArrayList with maps that needs splitting
	 * @param keyString String based on which the splitting of items are done
	 * @return Pair of maps
	 */
	public static Pair<ArrayList<LinkedHashMap<String, String>>, ArrayList<LinkedHashMap<String, String>>> getBifurcatedMaps(
			ArrayList<LinkedHashMap<String, String>> mapList, String keyString) {
		ArrayList<LinkedHashMap<String, String>> hasMatchList = new ArrayList<LinkedHashMap<String, String>>();
		ArrayList<LinkedHashMap<String, String>> noMatchList = new ArrayList<LinkedHashMap<String, String>>();
		for (LinkedHashMap<String, String> eachCtRecord : mapList) {
			if (eachCtRecord.get(keyString).trim().length() > 0) {
				hasMatchList.add(eachCtRecord);
			} else {
				noMatchList.add(eachCtRecord);
			}
		}
		return Pair.with(hasMatchList, noMatchList);
	}

	/**
	 * Returns the index with the highest match of a String in a list of Strings.
	 * The list of Strings is got from a HashMap with a key as hashKey.
	 * 
	 * @param searchString    String to search
	 * @param hashKey         key used to get corresponding value from the List of
	 *                        record maps
	 * @param toSearchRecords List of maps to be searched
	 * @return highest match index
	 */
	public static int getHighestMatchIndex(String searchString, String hashKey,
			ArrayList<LinkedHashMap<String, String>> toSearchRecords) {
		double highestMatch = 0.0;
		double match = 0.0;
		int returnIdx = -1;
		for (int i = 0; i < toSearchRecords.size(); i++) {
			String currentString = toSearchRecords.get(i).get(hashKey).trim();
			match = IsotropicStringSimilarity.similarity(StringUtil.getNormalizedUpCaseString(searchString),
					StringUtil.getNormalizedUpCaseString(currentString));
			// update index and highest match value
			if (match > highestMatch) {
				returnIdx = i;
				highestMatch = match;
			}
		}
		return returnIdx;
	}

	/**
	 * An utility to take a list of maps and sort them. The maps have been added in
	 * random order. This method uses the value of one key (like sequence, priority)
	 * within the map based on which sorting is done. If any of the values within
	 * the key is same, for example two keys have the value of priority=2, then the
	 * sorting does not change the order.
	 * 
	 * @param listOfMaps
	 * @param sortingKey
	 * @return
	 */
	public static List<Map<String, String>> sortListofMaps(List<Map<String, String>> listOfMaps, String sortingKey) {
		Comparator<Map<String, String>> mapComparator = new Comparator<Map<String, String>>() {
			public int compare(Map<String, String> m1, Map<String, String> m2) {
				return m1.get("priority").compareTo(m2.get("priority"));
			}
		};

		Collections.sort(listOfMaps, mapComparator);
		return listOfMaps;
	}
}
