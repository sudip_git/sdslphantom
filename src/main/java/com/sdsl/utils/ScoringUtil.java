/**
 * Copyright (C) 2018, Sumyag Data Sciences Pvt. Ltd - www.sumyag.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Sumyag Data Sciences Private Limited, 
   associated partners and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

 */
package com.sdsl.utils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.javatuples.Pair;
import org.json.simple.parser.ParseException;

import com.isotropic.utils.IsotropicStringSimilarity;
import com.sdsl.binning.GsonUtils;
import com.sdsl.binning.MetaDataParams;
import com.sdsl.binning.ScoringParams;

/**
 * @author Sudip Das
 *
 */
/**
 * @author Sudip Das
 *
 */
public class ScoringUtil {

	/**
	 * Scoring function that provides indices of records with highest match. Sorting
	 * is done based on highest score. Score is built with weighted average.
	 * 
	 * @param ctRecordsList   Current Term records
	 * @param ptPrRecordsList Prior Term records
	 * @param qs_Key          Checklist Question
	 * @return A List with Maps of Current Term and Prior Term indices with score
	 *         based pairing of the indices.
	 */
	public static List<LinkedHashMap<String, Integer>> getScoredMlRecords(
			ArrayList<LinkedHashMap<String, String>> ctRecordsList,
			ArrayList<LinkedHashMap<String, String>> ptPrRecordsList, String qs_Key,
			Map<String, String> slMlMetaConfigMaps, List<Map<String, String>> slMlScoreConfigParams,
			List<Map<String, String>> slMlCompareConfigMaps) {

		/*
		 * The return map with sorted records based on score and indices for those
		 * records
		 */
		List<LinkedHashMap<String, Integer>> matchedIndicesList = new ArrayList<LinkedHashMap<String, Integer>>();
		/*
		 * Get all the scoring params List, a dependency map with each scoring param as
		 * key and corresponding dependency as value, a weight map with each scoring
		 * param as key and corresponding weight as value.
		 * 
		 * Next get one record from CT side and score against each PTPR record. Loop
		 * through the entire scoring params list. If a param is dependent then it is
		 * considered for scoring.
		 * 
		 * For each dependent param we get the value from CT and PTPR side, get the
		 * match value. Compare it against the compare threshold from the slml compare
		 * params map. If value surpasses or equals threshold, the match value is
		 * retained else it is marked as zero.
		 * 
		 * When a param is dependent or considered, then the weighted score is
		 * calculated and the weighted score is appended to existing weighted score
		 * variable and the weight of the current param is appended to the total weight
		 * variable. All the scoring params are looped in this fashion and the weighted
		 * score variable is similarly appended and so is the total weight variable.
		 * 
		 * Next final Weighted Average Score is calculated by dividing the weighted
		 * score with total weight. If this value is greater than the final Scoring
		 * Threshold as got from the meta data params of the question, then it is added
		 * to the unsorted map.
		 * 
		 * Final step is to sort the map based on match value and return the HashMap.
		 */
		ArrayList<String> scoringParamsListForQs = new ArrayList<String>();
		LinkedHashMap<String, Boolean> dependencyMap = new LinkedHashMap<String, Boolean>();
		LinkedHashMap<String, Double> weightMap = new LinkedHashMap<String, Double>();
		for (Map<String, String> eachScoreParam : slMlScoreConfigParams) {
			scoringParamsListForQs.add(eachScoreParam.get("id"));
			dependencyMap.put(eachScoreParam.get("id"), Boolean.parseBoolean(eachScoreParam.get("dependency")));
			weightMap.put(eachScoreParam.get("id"), Double.parseDouble(eachScoreParam.get("weight")));
		}

//		System.err.println(slMlCompareConfigMaps);
//		System.err.println("dependencyMap:" + dependencyMap);
//		System.err.println("weightMap:" + weightMap);

		/*
		 * Map to hold records whose match value is greater than the final score
		 * threshold but not sorted in match descending
		 */
		LinkedHashMap<Pair<Integer, Integer>, Double> unsortedMap = new LinkedHashMap<Pair<Integer, Integer>, Double>();

		for (int i = 0; i < ctRecordsList.size(); i++) {
			// Get CT record map
			LinkedHashMap<String, String> tempCtMap = ctRecordsList.get(i);
			for (int j = 0; j < ptPrRecordsList.size(); j++) {
				// Get PTPR record map
				LinkedHashMap<String, String> tempPtPrMap = ptPrRecordsList.get(j);

				// Initialize the weight score to zero before we find the value match and
				// multiply by weight for a param based on dependecny
				double weightedScore = 0.0;
				// Initialize the total Weight for each param considered based on dependency
				double totalWeight = 0.0;

//				Read each scoring param, get applicable weight and whether dependent or not
				for (String eachScoringParam : scoringParamsListForQs) {
					double applicableParamWeight = weightMap.get(eachScoringParam);
					boolean isDependentOnParam = dependencyMap.get(eachScoringParam);

					// Initialize the match as 0
					double currentParamValMatch = 0.0;
					// Check if this is a dependent param for scoring from isDependentOnParam.
					if (isDependentOnParam) {
						// if this is a dependent param then read the value for this scoring param from
						// both CT and PTPT sides
						// and calculate the match
						String ctScoreValStr = StringUtil.getNormalizedUpCaseString(tempCtMap.get(eachScoringParam));
						String ptPrScoreValStr = StringUtil
								.getNormalizedUpCaseString(tempPtPrMap.get(eachScoringParam));
						currentParamValMatch = IsotropicStringSimilarity.similarity(ctScoreValStr, ptPrScoreValStr);

//						System.err.println("CT " + eachScoringParam + " value:" + ctScoreValStr);
//						System.err.println("PT " + eachScoringParam + " value:" + ptPrScoreValStr);
//
//						System.err.println("applicableParamWeight:" + applicableParamWeight);
//						System.err.println("isDependentOnParam:" + isDependentOnParam);
//						System.err.println("currentParamValMatch:" + currentParamValMatch);

						// Need to get the comparison threshold for this dependent scoring param
						for (Map<String, String> eachCompareParamMap : slMlCompareConfigMaps) {
							// Match the param name from id and get the compare threshold for match from
							// slMlCompareConfigMaps
							if (eachScoringParam.equalsIgnoreCase(eachCompareParamMap.get("id"))) {
								double paramValThreshold = Double
										.parseDouble(eachCompareParamMap.get("compareThreshold"));
								// if the match is less than compare threshold then pass zero else actual match
								// value
								currentParamValMatch = currentParamValMatch < paramValThreshold ? 0.0
										: currentParamValMatch;
								// This is a dependent param to increment weightedScore and totalWeight
								// Get the previous weighted score and add the match times its applicable weight
								weightedScore = weightedScore + currentParamValMatch * applicableParamWeight;
								totalWeight = totalWeight + applicableParamWeight;
							}
						}
//						System.err.println("weightedScore:" + weightedScore);
//						System.err.println("totalWeight:" + totalWeight);
					}
				}
				// weighted average is total weighted score of all dependent params divided by
				// totalWeight of all dependent params
				double finalWtAvgScore = weightedScore / totalWeight;
//				System.err.println("wtAvgScore:" + wtAvgScore);
				// if the average score is greater than equal to score threshold
				if (finalWtAvgScore >= Double.parseDouble(slMlMetaConfigMaps.get("finalScoringThreshold"))) {
					unsortedMap.put(Pair.with(i, j), finalWtAvgScore);
				}

//				if (qs_Key.equalsIgnoreCase("IG1")) {
//					System.out
//							.println("ctDesc=" + tempCtMap.get("qs_desc") + ", ptPrDesc=" + tempPtPrMap.get("qs_desc"));
//					System.err.println("ctValStd=" + tempCtMap.get("qs_value") + ", ptPrValStd="
//							+ tempPtPrMap.get("qs_value") + ", WtMatch=" + wtAvgScore);
//				}
			}
		}

		// Sorting the unsorted map
		LinkedHashMap<Pair<Integer, Integer>, Double> sortedMap = new LinkedHashMap<>();

		unsortedMap.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.forEachOrdered(x -> sortedMap.put(x.getKey(), x.getValue()));

		List<Integer> ctIdices = new ArrayList<Integer>();
		List<Integer> ptPrIdices = new ArrayList<Integer>();
		int ctIdx = -1;
		int ptPrIdx = -1;
		for (Pair<Integer, Integer> mapKey : sortedMap.keySet()) {
			LinkedHashMap<String, Integer> matchedKeysMap = new LinkedHashMap<String, Integer>();
			ctIdx = mapKey.getValue0();
			ptPrIdx = mapKey.getValue1();
			if (!ctIdices.contains(ctIdx)) {
				if (!ptPrIdices.contains(ptPrIdx)) {
					matchedKeysMap.put("ctIdx", ctIdx);
					matchedKeysMap.put("ptPrIdx", ptPrIdx);
					matchedIndicesList.add(matchedKeysMap);
					ctIdices.add(ctIdx);
					ptPrIdices.add(ptPrIdx);

				}
			}
		}

		return matchedIndicesList;
	}

	/**
	 * Scoring function that provides indices of records with highest match. Sorting
	 * is done based on highest score. Score is built with weighted average.
	 * 
	 * @param ctRecordsList   Current Term records
	 * @param ptPrRecordsList Prior Term records
	 * @param qs_Key          Checklist Question
	 * @return A List with Maps of Current Term and Prior Term indices with score
	 *         based pairing of the indices.
	 */
	public static List<LinkedHashMap<String, Integer>> getScoreHash(
			ArrayList<LinkedHashMap<String, String>> ctRecordsList,
			ArrayList<LinkedHashMap<String, String>> ptPrRecordsList, String qs_Key, boolean isLobDependent,
			boolean isDescMatch, double qsThreshold, double descThreshold, double descWt, double lobWt, double stdValWt,
			double avgThreshold) {

		List<LinkedHashMap<String, Integer>> matchedIndicesList = new ArrayList<LinkedHashMap<String, Integer>>();
		LinkedHashMap<Pair<Integer, Integer>, Double> unsortedMap = new LinkedHashMap<Pair<Integer, Integer>, Double>();

		for (int i = 0; i < ctRecordsList.size(); i++) {
			LinkedHashMap<String, String> tempCtMap = ctRecordsList.get(i);
//			String ctDesc = StringUtil.getNormalizedUpCaseString(tempCtMap.get("qs_desc"));
			String ctDesc = StringUtil.getNormalizedUpCaseString(tempCtMap.get("qs_desc_std"));
			String ctValStd = StringUtil.getNormalizedUpCaseString(tempCtMap.get("qs_value_std"));
			String ctLob = StringUtil.getNormalizedUpCaseString(tempCtMap.get("extr_lob"));

			for (int j = 0; j < ptPrRecordsList.size(); j++) {

				LinkedHashMap<String, String> tempPtPrMap = ptPrRecordsList.get(j);
//				String ptPrDesc = StringUtil.getNormalizedUpCaseString(tempPtPrMap.get("qs_desc"));
				String ptPrDesc = StringUtil.getNormalizedUpCaseString(tempPtPrMap.get("qs_desc_std"));
				String ptPrValStd = StringUtil.getNormalizedUpCaseString(tempPtPrMap.get("qs_value_std"));
				String ptPrLob = StringUtil.getNormalizedUpCaseString(tempPtPrMap.get("extr_lob"));

				double descMatch = IsotropicStringSimilarity.similarity(ctDesc, ptPrDesc);
				double lobMatch = IsotropicStringSimilarity.similarity(ctLob, ptPrLob);
				double stdValMatch = IsotropicStringSimilarity.similarity(ctValStd, ptPrValStd);

				if (isDescMatch) {
					descMatch = descMatch < descThreshold ? 0.0 : descMatch;
				}
				if (isLobDependent) {
					lobMatch = ctLob.equalsIgnoreCase(ptPrLob) ? lobMatch : 0.0;
				}

				stdValMatch = stdValMatch < qsThreshold ? 0.0 : stdValMatch;

				double wtAvgScore = (descMatch * descWt + lobMatch * lobWt + stdValMatch * stdValWt)
						/ (descWt + lobWt + stdValWt);

				// avgThreshold added this parameter instead of calculating while segregating
				// code
//				if (wtAvgScore > Double.parseDouble((String) ConfigLoader.avgThresholdProps.getProperty(qs_Key))) 
				if (wtAvgScore > avgThreshold) {
					unsortedMap.put(Pair.with(i, j), wtAvgScore);
				}

//				if (qs_Key.equalsIgnoreCase("IG1")) {
//					System.out.println("*************************************************************************");
//					System.out.println("ctDesc="+tempCtMap.get("qs_desc")+", ptPrDesc="+tempPtPrMap.get("qs_desc"));
//					System.err.println("ctValStd=" + tempCtMap.get("qs_value") + ", ptPrValStd="
//							+ tempPtPrMap.get("qs_value") + ", VALUE MATCH:" + stdValMatch + ", WtMatch=" + wtAvgScore);
//				}
			}
		}
		LinkedHashMap<Pair<Integer, Integer>, Double> sortedMap = new LinkedHashMap<>();

		unsortedMap.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.forEachOrdered(x -> sortedMap.put(x.getKey(), x.getValue()));

		List<Integer> ctIdices = new ArrayList<Integer>();
		List<Integer> ptPrIdices = new ArrayList<Integer>();
		int ctIdx = -1;
		int ptPrIdx = -1;
		for (Pair<Integer, Integer> mapKey : sortedMap.keySet()) {
			LinkedHashMap<String, Integer> matchedKeysMap = new LinkedHashMap<String, Integer>();
			ctIdx = mapKey.getValue0();
			ptPrIdx = mapKey.getValue1();
			if (!ctIdices.contains(ctIdx)) {
				if (!ptPrIdices.contains(ptPrIdx)) {
					matchedKeysMap.put("ctIdx", ctIdx);
					matchedKeysMap.put("ptPrIdx", ptPrIdx);
					matchedIndicesList.add(matchedKeysMap);
					ctIdices.add(ctIdx);
					ptPrIdices.add(ptPrIdx);

				}
			}
		}

		return matchedIndicesList;
	}

	/**
	 * 
	 * Scoring function that provides indices of records with highest match. Sorting
	 * is done based on highest score. Score is built with weighted average.
	 * 
	 * @param ctRecordsList   Current Term records
	 * @param ptPrRecordsList Prior Term records
	 * @param binUtil         Binning Utility Object to get MetaData and Scoring
	 *                        Parameters
	 * @return A List with Maps of Current Term and Prior Term indices with score
	 *         based pairing of the indices.
	 * @throws ParseException
	 */
	public static List<LinkedHashMap<String, Integer>> getScoreHashBin(
			ArrayList<LinkedHashMap<String, String>> ctRecordsList,
			ArrayList<LinkedHashMap<String, String>> ptPrRecordsList, BinningUtil binUtil) throws ParseException {

		List<LinkedHashMap<String, Integer>> matchedIndicesList = new ArrayList<LinkedHashMap<String, Integer>>();
		LinkedHashMap<Pair<Integer, Integer>, Double> unsortedMap = new LinkedHashMap<Pair<Integer, Integer>, Double>();

		MetaDataParams metaDataParamsObj = binUtil.getMetaDataParamsObj();
		ScoringParams scoreParamsObj = binUtil.getScoreParamsObj();
		// For scoring get the scoring keys using Binning Util
		List<String> scoreTemplateParamKeys = GsonUtils.getBinJsonTemplateKeys(scoreParamsObj);

		HashMap<String, Double> scoringWeightsMap = getDistributeWeights(scoreTemplateParamKeys, "lob");
//		double totalWeight = 1.0;
		// 6.x change
//		double avgThreshold = Double.valueOf(metaDataParamsObj.getMetaDataParams().get("avgScoringThreshold"));
		double avgThreshold = Double.valueOf(metaDataParamsObj.getMetaDataParams().get("finalScoringThreshold"));

		for (int i = 0; i < ctRecordsList.size(); i++) {
			LinkedHashMap<String, String> tempCtMap = ctRecordsList.get(i);
			for (int j = 0; j < ptPrRecordsList.size(); j++) {
				LinkedHashMap<String, String> tempPtPrMap = ptPrRecordsList.get(j);
				double weightedMatch = 0.0;
				for (String eachScoringKey : scoreTemplateParamKeys) {
					double match = IsotropicStringSimilarity.similarity(tempCtMap.get(eachScoringKey),
							tempPtPrMap.get(eachScoringKey));
//					System.err.println(tempCtMap.get(eachScoringKey)+":"+tempPtPrMap.get(eachScoringKey)+"="+match);
//					System.err.println("Initial->"+tempCtMap.get(eachScoringKey)+":"+tempPtPrMap.get(eachScoringKey)+"="+weightedMatch);
					weightedMatch = weightedMatch + match * scoringWeightsMap.get(eachScoringKey);
//					System.err.println("Final->"+tempCtMap.get(eachScoringKey)+":"+tempPtPrMap.get(eachScoringKey)+"="+weightedMatch);
//					System.err.println("ScoringUtil.getScoreHashBin() Score Key:"+eachScoringKey+"="+weightedMatch);
				}
				double wtAvgScore = weightedMatch / scoreTemplateParamKeys.size();
//				System.err.println("ScoringUtil.getScoreHashBin()wtAvgScore:"+wtAvgScore);
				if (wtAvgScore > avgThreshold) {
					unsortedMap.put(Pair.with(i, j), wtAvgScore);
				}
			}
		}

//		System.err.println("ScoringUtil.getScoreHashBin() unsortedMap:"+unsortedMap);

		LinkedHashMap<Pair<Integer, Integer>, Double> sortedMap = new LinkedHashMap<>();

		unsortedMap.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.forEachOrdered(x -> sortedMap.put(x.getKey(), x.getValue()));

//		System.err.println("ScoringUtil.getScoreHashBin() sortedMap:"+sortedMap);

		List<Integer> ctIdices = new ArrayList<Integer>();
		List<Integer> ptPrIdices = new ArrayList<Integer>();
		int ctIdx = -1;
		int ptPrIdx = -1;
		for (Pair<Integer, Integer> mapKey : sortedMap.keySet()) {
			LinkedHashMap<String, Integer> matchedKeysMap = new LinkedHashMap<String, Integer>();
			ctIdx = mapKey.getValue0();
			ptPrIdx = mapKey.getValue1();
			if (!ctIdices.contains(ctIdx)) {
				if (!ptPrIdices.contains(ptPrIdx)) {
					matchedKeysMap.put("ctIdx", ctIdx);
					matchedKeysMap.put("ptPrIdx", ptPrIdx);
					matchedIndicesList.add(matchedKeysMap);
					ctIdices.add(ctIdx);
					ptPrIdices.add(ptPrIdx);

				}
			}
		}

//		System.err.println("ScoringUtil.getScoreHashBin() matchedIndicesList:"+matchedIndicesList);

		return matchedIndicesList;
	}

	/**
	 * Distribute weights for all parameters passed
	 * 
	 * @param paramKeys   List of parameters
	 * @param roundOffKey parameter where residue weight after distribution is
	 *                    asssigned
	 * @return Map of param with the weight assigned
	 */
	public static HashMap<String, Double> getDistributeWeights(List<String> paramKeys, String roundOffKey) {
		HashMap<String, Double> paramWeights = new HashMap<String, Double>();

		int number = paramKeys.size();
		double distribute = 1.0 / number;
		distribute = Math.round(distribute * 100.0) / 100.0;

		if ((number & 1) == 0) {
			for (String eachKey : paramKeys) {
				paramWeights.put(eachKey, distribute);
			}
		} else {
			double oddTotal = 0.0;
			for (String eachKey : paramKeys) {
				paramWeights.put(eachKey, distribute);
				oddTotal = oddTotal + distribute;
			}
			double residual = distribute + 1.0 - oddTotal;
			residual = Math.round(residual * 100.0) / 100.0;
			if (StringUtil.containsStringIgnoreCase(roundOffKey, new ArrayList<String>(paramKeys))) {
				paramWeights.put(roundOffKey, residual);
			}
		}
		return paramWeights;
	}

}
