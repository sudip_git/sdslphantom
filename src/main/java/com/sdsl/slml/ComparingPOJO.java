/**
 * 
 */
package com.sdsl.slml;

import java.util.List;
import java.util.Map;

/**
 * @author Sudip Das
 *
 */
public class ComparingPOJO {
	private static final String SLML_COMPAREPARAMS_KEYSTR = "slmlCompareParamsKey";

	private List<Map<String, String>> comparisonParams;

	public List<Map<String, String>> getComparisonParams() {
		return comparisonParams;
	}

	public void setComparisonParams(List<Map<String, String>> comparisonParams) {
		this.comparisonParams = comparisonParams;
	}

	public static String getSlmlCompareparamsKeystr() {
		return SLML_COMPAREPARAMS_KEYSTR;
	}

}
