/**
 * 
 */
package com.sdsl.slml;

import java.util.List;
import java.util.Map;

/**
 * @author Sudip Das
 *
 */
public class ScoringPOJO {
	private static final String SLML_SCOREPARAMS_KEYSTR = "slmlScoreParamsKey";

	private List<Map<String, String>> scoringParams;

	public List<Map<String, String>> getScoringParams() {
		return scoringParams;
	}

	public void setScoringParams(List<Map<String, String>> scoringParams) {
		this.scoringParams = scoringParams;
	}

	public static String getSlmlScoreparamsKeystr() {
		return SLML_SCOREPARAMS_KEYSTR;
	}

}
