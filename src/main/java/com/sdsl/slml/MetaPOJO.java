/**
 * 
 */
package com.sdsl.slml;

import java.util.Map;

/**
 * @author Sudip Das
 *
 */
public class MetaPOJO {
	private static final String SLML_METAPARAMS_KEYSTR = "slmlMetaParamsKey";

	private Map<String, String> metaDataParams;

	public Map<String, String> getMetaDataParams() {
		return metaDataParams;
	}

	public void setMetaDataParams(Map<String, String> metaDataParams) {
		this.metaDataParams = metaDataParams;
	}

	public static String getSlmlMetaparamsKeystr() {
		return SLML_METAPARAMS_KEYSTR;
	}
}
