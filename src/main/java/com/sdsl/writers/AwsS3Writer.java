/**
 * Copyright (C) 2018, Sumyag Data Sciences Pvt. Ltd - www.sumyag.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Sumyag Data Sciences Private Limited, 
   associated partners and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

 */
package com.sdsl.writers;

import java.io.File;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

/**
 * @author Sudip Das
 *
 */
public class AwsS3Writer {

	public AwsS3Writer() {
	}

	/**
	 * Method to write excel file in S3 bucket
	 * 
	 * @param fileToUploadWithPath File To Upload With Path (Input)
	 * @param jobID                Job ID
	 * @param s3Path               S3 Bucket Path
	 * @param ouptFileName         File Name in S3 (Output)
	 */
	public static void writeToS3(String accessKey, String secKey, Regions region, String contentType,
			String fileToUploadWithPath, String jobID, String s3Path, String ouptFileName) {

		BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secKey);
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(region)
				.withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();

		PutObjectRequest request = new PutObjectRequest(s3Path, ouptFileName, new File(fileToUploadWithPath));
		ObjectMetadata objectMetaData = new ObjectMetadata();
		objectMetaData.setContentType(contentType);
		request.setMetadata(objectMetaData);
		s3Client.putObject(request);
	}

	/**
	 * Method to write log files as text files in S3 bucket
	 * 
	 * @param fileToUploadWithPath File To Upload With Path (Input)
	 * @param jobID                Job ID
	 * @param s3Path               S3 Bucket Path
	 * @param ouptFileName         File Name in S3 (Output)
	 */

	public static void writeLogToS3(String accessKey, String secKey, Regions region, String contentType,
			String fileToUploadWithPath, String jobID, String s3Path, String ouptFileName) {
		// Writing job log file to s3

		BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secKey);
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(region)
				.withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();

		PutObjectRequest request = new PutObjectRequest(s3Path, ouptFileName, new File(fileToUploadWithPath));
		ObjectMetadata objectMetaData = new ObjectMetadata();
		objectMetaData.setContentType(contentType);
		request.setMetadata(objectMetaData);
		s3Client.putObject(request);
	}

	public static void main(String[] args) {
	}
}
