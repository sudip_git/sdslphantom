/**
 * Copyright (C) 2018, Isotropic Solutions Pvt.Ltd. www.isotropicsolutions.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Isotropic Solutions Private Limited
   and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

 */

package com.isotropic.utils;

import com.sdsl.utils.StringUtil;

/**
 * @author Sudip Das
 *
 */
public class IsotropicStringSimilarity {

	/**
	 * Calculates the similarity (a number within 0 and 1) between two strings.
	 * 
	 * @param s1 First String
	 * @param s2 Second String
	 * @return match score
	 */
	public static double similarity(String s1, String s2) {
		String longer = s1, shorter = s2;
		if (s1.length() < s2.length()) { // longer should always have greater length
			longer = s2;
			shorter = s1;
		}
		int longerLength = longer.length();
		if (longerLength == 0) {
			return 1.0;
			/* both strings are zero length */ }

		return ((longerLength - editDistance(longer, shorter)) / (double) longerLength) * 100;

	}

	/**
	 * Distance calculation between strings
	 * 
	 * @param s1 First String
	 * @param s2 Second String
	 * @return calculated distance
	 */
	public static int editDistance(String s1, String s2) {
		s1 = s1.toLowerCase();
		s2 = s2.toLowerCase();

		int[] costs = new int[s2.length() + 1];
		for (int i = 0; i <= s1.length(); i++) {
			int lastValue = i;
			for (int j = 0; j <= s2.length(); j++) {
				if (i == 0)
					costs[j] = j;
				else {
					if (j > 0) {
						int newValue = costs[j - 1];
						if (s1.charAt(i - 1) != s2.charAt(j - 1))
							newValue = Math.min(Math.min(newValue, lastValue), costs[j]) + 1;
						costs[j - 1] = lastValue;
						lastValue = newValue;
					}
				}
			}
			if (i > 0)
				costs[s2.length()] = lastValue;
		}
		return costs[s2.length()];
	}

	/**
	 * Print String similarity
	 * 
	 * @param s First String
	 * @param t Second String
	 */
	public static void printLevenshteinSimilarity(String s, String t) {
//		logger.debug(String.format("%.0f%% is the similarity between \"%s\" and \"%s\"", similarity(s, t), s, t));
		System.out.println(String.format("%.0f%% is the similarity between \"%s\" and \"%s\"", similarity(s, t), s, t));
		// System.out.println(String.format("%.0f%% is the similarity between \"%s\" and
		// \"%s\"", similarity(s, t), s, t));
	}

	public static void main(String[] args) {
		// printLevenshteinSimilarity("", "");
		// printLevenshteinSimilarity("1234567890", "1");
		// printLevenshteinSimilarity("1234567890", "123");
		// printLevenshteinSimilarity("1234567890", "1234567");
		// printLevenshteinSimilarity("1234567890", "1234567890");
		// printLevenshteinSimilarity("1234567890", "1234567980");
		// printLevenshteinSimilarity("47/2010", "472010");
		// printLevenshteinSimilarity("47/2010", "472011");
		// printLevenshteinSimilarity("47/2010", "AB.CDEF");
		// printLevenshteinSimilarity("47/2010", "4B.CDEFG");
		// printLevenshteinSimilarity("47/2010", "AB.CDEFG");
		// printLevenshteinSimilarity("The quick fox jumped", "The fox jumped");
		// printLevenshteinSimilarity("The quick fox jumped", "The fox");
		// printLevenshteinSimilarity("The quick fox jumped", "The quick fox jumped off
		// the balcany");
		String str1 = "IL8576(10/17)*";
		String str2 = "IL8576(09/09)*";
		System.out.println("Original Strings:" + str1 + " || " + str2);
		str1 = StringUtil.getNormalizedUpCaseString(str1);
		str2 = StringUtil.getNormalizedUpCaseString(str2);
		System.out.println("Modified Strings:" + str1 + " || " + str2);
		System.err.println("match=" + similarity(str1, str2));
		printLevenshteinSimilarity(str1, str2);
		// printLevenshteinSimilarity("Vishy", "Vishy");
	}

}
